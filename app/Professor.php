<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
  //One-to-many
  public function title() {
        return $this->belongsTo('App\Title');
   }

  //One-to-one
	public function user() {
        return $this->belongsTo('App\User');
   }

  //One-to-many
   public function department() {
        return $this->belongsTo('App\Department');
   }

  //Many-to-many
   public function subjects()
    {
        return $this->belongsToMany('App\Subject')->withPivot('norm', 'enrollment_year');
    }

  //One-to-many
    public function grades() {
        return $this->hasMany('App\Grade');
   }
}

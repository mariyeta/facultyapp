<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //One-to-one
    public function user() {
        return $this->belongsTo('App\User');
   }

    //One-to-many
   public function module()
    {
        return $this->belongsTo('App\Module');
    }

    //Many-to-many
     public function subjects()
    {
        return $this->belongsToMany('App\Subject');
    }

    /*
    //Many-to-many
    public function exams()
    {
        return $this->belongsToMany('App\Exam');
    }
    */

     //One-to-many
   public function enrollments()
    {
        return $this->hasMany('App\Enrollment');
    }

    //One-to-many
    public function examStudents()
    {
        return $this->hasMany('App\ExamStudent', 'student_id', 'id');
    }


    public function scopeExamDate($query)
    {
        return $query->orderBy('exam_date', 'asc');
    }
}



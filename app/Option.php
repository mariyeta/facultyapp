<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Cache;

class Option extends Model
{
	public $timestamps = false;

    //One-to-many
    public function role() {
    	return $this->belongsTo('App\Role');
    }

    //vraca opcije za sidebar iz baze u zavisnosti od uloge korisnika
    //ova promenljiva se zauvek cuva za svaku ulogu u cache tako da se bazi pristupa samo prvi put
    public static function sidebar() {
    		$option = Cache::rememberForever('sidebar'.auth()->user()->role_id, function() {
				  return static::where('role_id', auth()->user()->role_id)->get();
			});
    	return $option;
    }

}

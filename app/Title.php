<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Title extends Model
{  
	public $timestamps = false;

	//One-to-many
    public function professors() {
        return $this->hasMany('App\Professor');
   }
}


@extends('layouts.app')

@section('content')
@if(Auth::check()) 

  <h1 class="text-center">O profesoru</h1>
  <hr>
  <div class="row">
     <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center">
        <img class="resize" src="{{ asset( $professor->user->img_path.$professor->user->img_name) }}">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <table class="table table-bordered text-left">
          <tbody>
            <tr>
              <td>Ime i prezime</td>
              <td>{{ $professor->first_name }} {{ $professor->last_name }}</td>
            </tr>
            <tr>
              <td>Ime jednog roditelja</td>
              <td>{{ $professor->parent_name }}</td>
            </tr>
            <tr>
              <td>Pol</td>
              <td>{{ $professor->gender == "ž" ? "ženski" : "muški" }}</td>
            </tr>
            <tr>
              <td>Datum rođenja</td>
              <td>{{ Carbon\Carbon::parse($professor->date_of_birth)->format('d.m.Y.') }}</td>
            </tr>
            <tr>
              <td>JMBG</td>
              <td>{{ $professor->jmbg }}</td>
            </tr>
            <tr>
              <td>Adresa</td>
              <td>{{ $professor->street_address }}</td>
            </tr>
            <tr>
              <td>E-mail</td>
              <td>{{ $professor->user->email }}</td>
            </tr>
            <tr>
              <td>Odsek</td>
              <td>{{ $professor->department->name }}</td>
            </tr>
            <tr>
              <td>Zvanje</td>
              <td>{{ $professor->title->name }}</td>
            </tr>
            <tr>
              <td>Datum sticanja zvanja</td>
              <td>{{ Carbon\Carbon::parse($professor->from_date)->format('d.m.Y.') }}</td>
            </tr>
          </tbody>
        </table>
      </div>
  </div>
 
@endif
@endsection
   
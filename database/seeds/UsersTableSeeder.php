<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = App\Role::orderBy('name')->get();

        //kreiramo po 6 korisnika za svaku ulogu, dodeljujemo im inkrementovane lozinke i korisnicka imena
        //email i random token se dodeljuju pomocu patterna definisanog u factory-u u ModelFactory
        for($i=1; $i<=6; $i++) {
                factory(App\User::class)->create(array('role_id' => $roles[0]->id, 'name' => "Admin".$i, 'password' => bcrypt("apassword".$i)));
                factory(App\User::class)->create(array('role_id' => $roles[1]->id, 'name' => "Prof".$i, 'password' => bcrypt("ppassword".$i)));
                factory(App\User::class)->create(array('role_id' => $roles[2]->id, 'name' => "Student".$i, 'password' => bcrypt("spassword".$i)));
        }
    }
}


@extends('layouts.app')

@section('content')
@if(Auth::check()) 

  <h1 class="text-center">Upisi</h1>
  <hr>
  <div class="row">
    <div class="col-12">
      <table class="table table-bordered text-left subject_table">
        <tbody>
          <tr>
            <th class="subject_width1" title="Skolska godina">Skolska godina</th>
            <th class="subject_width1" title="Studentska godina">Godina</th>
            <th class="subject_width1" title="Obnova">Obnova</th>
            <th class="subject_width1" title="Smer">Smer</th>
            <th class="subject_width1" title="Datum upisa">Datum</th>
          </tr>
          @foreach($student->enrollments as $enrollment)
          <tr>
            <td>{{ Carbon\Carbon::parse($enrollment->enrollment_date)->format('Y')."/".Carbon\Carbon::parse($enrollment->enrollment_date)->addYear()->format('Y') }}</td>
            <td>{{ $enrollment->student_year }}</td>
            <td>{{ $enrollment->renewal == 0 ? "Ne" : "Da" }}</td>
            <td>{{ $student->module->name }}</td>
            <td>{{ $enrollment->enrollment_date }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
    
@endif
@endsection
   

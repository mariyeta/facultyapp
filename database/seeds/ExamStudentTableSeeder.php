<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ExamStudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    

    public function run()
    {

        $students = App\Student::all();

        //za svakog studenta
        foreach($students as $student) {
            $index_year = intval(substr($student->index_number, 0, 4));
           
            //za svaku od 4 godine nalazimo sve predmete studenta
            for($i=0; $i<4; $i++) {
                $subjects[$i] = DB::table('student_subject')->join('subjects', 'subjects.id', '=', 'student_subject.subject_id')->where('student_subject.student_id', $student->id)->where('subjects.student_year', $i+1)->pluck('subjects.id')->toArray();
        	}

            //podaci o upisima svake skolske godine
            $enrollments = DB::table('enrollments')->where('enrollments.student_id', $student->id)->select(DB::raw('enrollments.student_year AS year'), DB::raw('enrollments.enrollment_date AS date'), 'enrollments.renewal')->get();
        
            //za svaki upis
        	foreach($enrollments as $enrollment) {
        		$enrollment_year = Carbon::createFromFormat('Y-m-d', $enrollment->date)->year;

                //ako je godina upisa vec od godine iz indeksa (pretpostavka je da student prve godine upisa nije polagao ispite)
                if($enrollment_year > $index_year) {
                    //studentska godina (1,2,3,4)
            		$student_year = $enrollment->year;
                    //da li je obnova ili ne
                    $renewal = $enrollment->renewal;
                    $student_subjects = array();
                    //ukoliko nije obnova, biramo predmete iz prethodne godine (ukoliko student prvi put upisuje 3, do sada je polagao samo predmete iz 1 i 2)
                    //ukoliko je obnova, student je mogao da polaze i predmete iz te godine (ukoliko obnavlja 3, mogao je prosle godine da polaze i ispite iz 3)
                    if($renewal==0) {
            		  $student_subjects = $subjects[$student_year-2];
                    }
                    else {
                      $student_subjects = $subjects[$student_year-1];
                    }

                //Za svakog studenta, svake enrollment godine nakon prvog upisa (2014,2015...) pretpostavljamo da je izlazio na 2 do 5 ispita
                $randNumber = mt_rand(2,5);
                $exams = DB::table('exams')->whereIn('subject_id', $student_subjects)->where( DB::raw('YEAR(exam_date)'), '=', $enrollment_year)->orderByRaw('RAND()')->take($randNumber)->get();
                    foreach($exams as $exam) {
                        DB::table('exam_students')->insert([
                            'student_id' => $student->id,
                            'exam_id' => $exam->id
                        ]);
                    }

                }
        	}

        }
    	
    }


}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('parent_name');
            $table->date('date_of_birth');
            $table->string('jmbg', 13);
            $table->string('street_address');
            $table->string('gender', 1);
            $table->tinyInteger('title_id')->unsigned();
            $table->date('from_date');
            $table->tinyInteger('department_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('professors', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('title_id')->references('id')->on('titles');
            $table->foreign('department_id')->references('id')->on('departments');
        });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professors');
    }
}

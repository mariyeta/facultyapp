<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
     use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
   
    //One-to-one
    public function student() {
        return $this->hasOne('App\Student');
   }

   //One-to-one
    public function professor() {
        return $this->hasOne('App\Professor');
   }

    //One-to-many
   public function role() {
        return $this->belongsTo('App\Role');
   }


   //proverava da li je korisnik ulogovan kao Administrator
   public function isAdmin() {
        //sa Eloquent relationships: $this->role()->where('name', 'Administrator')->first()
        if($this->role_id == 1) {
            return true;
        }
            return false;
    }

    //proverava da li je korisnik ulogovan kao Student
    public function isStudent() {
        if($this->role_id == 3) {
            return true;
        }
            return false;
    }

    //proverava da li je korisnik ulogovan kao Profesor
    public function isProfessor() {
        if($this->role_id == 2) {
            return true;
        }
            return false;
    }
   
}
<?php

namespace Faker\Provider;

class MyMethods extends Base
{
    
    //broj indeksa je godina rodjenja +19 / broj (cetiri cifre)
    //npr. 2012/0031
    public static function indexNumber($year)
    {
    	$index_year = (int)$year+19;
        return $index_year."/".str_pad(rand(1,1000), 4, '0', STR_PAD_LEFT);
    }


    //jmbg je dan rodjenja, mesec rodjenja, godina rodjenja (minus prva cifra), i random 6 cifara
    // za 23.08.1995. je 2308995rrrrrr
    public static function jmbg($dob)
    {
        return date('d',strtotime($dob)).date('m',strtotime($dob)).substr(date('Y',strtotime($dob)),1).rand(100000, 999999);
    }


}

<div class="col-6 col-md-3 sidebar-offcanvas" id="sidebar">
    <div class="list-group">
        @foreach ($options as $option)
           <a href="{{ $option->href }}" class="list-group-item">{{ $option->description }}</a>
        @endforeach
    </div>
</div>
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Professor;
use Cache;

class ProfessorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $professor;

     //Cacheramo osnovne podatke o profesoru
    public function __construct()
    {
        $this->middleware(function ($request, $next) {  
            $this->professor = Cache::remember('professor'.auth()->user()->id, 60, function () {
                return Professor::with('user', 'department', 'title')->where('professors.user_id', auth()->user()->id)->first();
            });
            return $next($request);
        });
    }

    //Pocetna strana za profesora
    public function index()
    {
        $name = $this->professor->first_name." ".$this->professor->last_name;
        return view('professor.main', compact('name'));
    }

    //Prikaz osnovnih podataka o profesoru
    public function personal()
    {     
        return view('professor.personal')->with('professor', $this->professor);
    }

    //Prethodna zvanja profesora
    public function previousTitles()
    {     
        $titles = Cache::remember('titles'.auth()->user()->id, 60, function () {
                return \DB::table('professor_history')->join('titles', 'professor_history.title_id', '=', 'titles.id')->where('professor_id', $this->professor->id)->select('professor_history.*', 'titles.name')->orderBy('from_date', 'asc')->get();
        });

        return view('professor.previous_titles', compact('titles'));
    }
}

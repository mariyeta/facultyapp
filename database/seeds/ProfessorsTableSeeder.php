<?php

use Illuminate\Database\Seeder;

class ProfessorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $count = App\Department::count();
        $faker = Faker\Factory::create();

        //za svaki department kreiramo po 6 profesora
        for($k=1; $k<=$count; $k++) { 
            factory(App\Professor::class, 6)->create(['department_id' => $k]);

            //nalazimo id-eve predmeta tog departmana
            $subjects = DB::table('subjects')->join('module_subject', 'subjects.id', '=', 'module_subject.subject_id')->join('modules', 'modules.id', '=', 'module_subject.module_id')->join('departments', 'departments.id', '=', 'modules.department_id')->where('departments.id', $k)->groupBy('subjects.id')->pluck('subjects.id')->toArray();
            
            //nalazimo profesore tog departmana
            $professors = App\Professor::where('department_id',$k)->pluck('id')->toArray();

            //norma profesora na predmetu
            $norm = [30.00, 40.00, 50.00, 60.00, 70.00];
        
            //za svaku godinu, za svaki predmet nalazimo po dva razlicita profesora da ga predaju
            //ne moraju isti predmet predavati isti profesori svaki skolske godine
            for($u=2012; $u<2017; $u++) 
            {
                for($j=0; $j<count($subjects); $j++) 
                {
                    shuffle($professors);
                    //ukupan zbir norme oba profesora mora biti 100% 
                    $first_norm = $norm[array_rand($norm)];
                    $second_norm = 100.00 - $first_norm;
                    DB::table('professor_subject')->insert([
                        [
                            'professor_id' => $professors[0],
                            'subject_id' => $subjects[$j],
                            'norm' => $first_norm,
                            'school_year' => $u
                        ],
                        [
                            'professor_id' => $professors[1],
                            'subject_id' => $subjects[$j],
                            'norm' => $second_norm,
                            'school_year' => $u
                        ],

                    ]);
                }
            }
        }

    	
    }
}


<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
  //One-to-many
	public function professor() {
        return $this->belongsTo('App\Professor');
   }

	//One-to-many
public function examStudents()
    {
        return $this->belongsTo('App\ExamStudent');
    }


}

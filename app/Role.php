<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Role extends Model
{
	public $timestamps = false;
	
	 //One-to-many
    public function users() {
    	return $this->hasMany('App\User');
    }

     //One-to-many
    public function options() {
    	return $this->hasMany('App\Option');
    }
}

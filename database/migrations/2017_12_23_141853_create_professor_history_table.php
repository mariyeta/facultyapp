<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessorHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professor_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('professor_id')->unsigned();
            $table->tinyInteger('title_id')->unsigned();
            $table->date('from_date');
            $table->date('to_date');
            $table->boolean('reappointment')->default(false);
            $table->timestamps();
        });

        Schema::table('professor_history', function($table) {
            $table->foreign('professor_id')->references('id')->on('professors');
            $table->foreign('title_id')->references('id')->on('titles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professor_history');
    }
}

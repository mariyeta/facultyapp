<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
	public $timestamps = false;
	
	//One-to-many
    public function modules() {
        return $this->hasMany('App\Module');
   }

	//One-to-many
     public function professors() {
        return $this->hasMany('App\Professor');
   }
}


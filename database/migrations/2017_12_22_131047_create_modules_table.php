<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->tinyInteger('id', true, true);
            $table->tinyInteger('department_id')->unsigned();
            $table->string('name');
        });

        Schema::table('modules', function($table) {
            $table->foreign('department_id')->references('id')->on('departments');
        });



            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
}


@extends('layouts.app')

@section('content')
@if(Auth::check()) 

  <h1 class="text-center">O studentu</h1>
  <hr>
  <div class="row">
     <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center">
        <img class="resize" src="{{ asset( $student->user->img_path.$student->user->img_name) }}">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <table class="table table-bordered text-left">
          <tbody>
            <tr>
              <td>Ime i prezime</td>
              <td>{{ $student->first_name }} {{ $student->last_name }}</td>
            </tr>
             <tr>
              <td>Broj indeksa</td>
              <td>{{ $student->index_number }}</td>
            </tr>
            <tr>
              <td>Ime jednog roditelja</td>
              <td>{{ $student->parent_name }}</td>
            </tr>
            <tr>
              <td>Pol</td>
              <td>{{ $student->gender == "ž" ? "ženski" : "muški"}}</td>
            </tr>
            <tr>
              <td>Datum rođenja</td>
              <td>{{ Carbon\Carbon::parse($student->date_of_birth)->format('d.m.Y.') }}</td>
            </tr>
            <tr>
              <td>JMBG</td>
              <td>{{ $student->jmbg }}</td>
            </tr>
            <tr>
              <td>Adresa</td>
              <td>{{ $student->street_address }}</td>
            </tr>
            <tr>
              <td>E-mail</td>
              <td>{{ $student->user->email }}</td>
            </tr>
            <tr>
              <td>Odsek</td>
              <td>{{ $student->module->department->name }}</td>
            </tr>
            <tr>
              <td>Smer</td>
              <td>{{ $student->module->name }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  
@endif
@endsection
   
<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class GradesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

         $students = App\Student::all();

         foreach($students as $student) {
            //podaci o polaganjima studenta
            $exams = DB::table('exam_students')->join('exams','exams.id','exam_students.exam_id')->where('student_id','=',$student->id)->orderBy('exam_date','desc')->select('exam_students.*', 'subject_id', 'exam_date')->get();
            $help = array();
            $professors = DB::table('professor_subject')->get();

            //za svako polaganje
            foreach($exams as $exam) { 

                //nalazimo profesore koji su te godine drzali taj predmet jer samo oni mogu da unesu ocenu
                //ako su ispiti iz 2013., skolska godina je pocela 2012 zbog toga ovo $year-1
                $year = Carbon::createFromFormat('Y-m-d H:i:s', $exam->exam_date)->year;
                $prof_subject = $professors->where('school_year', $year-1)->where('subject_id',$exam->subject_id)->pluck('professor_id')->toArray();
                shuffle($prof_subject);

                //moze se desiti da je student vise puta polagao isti predmet
                //ogranicavamo da ne moze da u jednom polaganju polozi, a u nekom kasnije padne isti predmet
                //posto proveravamo polaganja od poslednjeg ka prethodnim, oznacavamo da samo poslednji put kada je polagao taj ispt, student ga je mozda polozio
                //smestamo taj ispit u $help
                //ukoliko ispit vec postoji u $help, za prethodna polaganja dodeljujemo 0 (nije se pojavio) ili 5 (pao ispit)
                $rand_grades = array();
                if (in_array($exam->subject_id, $help)) {
                    $rand_grades = [0,5];
                } else {
                    $rand_grades = [5,6,7,8,9,10];
                }
                $help[] = $exam->subject_id;
                
                //Studentu dodeljujemo random ocenu
                $rand_key=array_rand($rand_grades);
                DB::table('grades')->insert([
                    'exam_student_id' => $exam->id,
                    'grade' => $rand_grades[$rand_key],
                    'professor_id' => $prof_subject[0]
                    ]);

            }
         }

    }

}
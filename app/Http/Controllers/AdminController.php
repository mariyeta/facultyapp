<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    //Pocetna stranica
    public function index()
    {
        return view('admin.main');
    }


    //Admin2 stranica se loaduje
     public function admin2()
    {
        return view('admin.admin2');
    }

}

<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon; 

class EnrollmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = App\Student::all();

    	$faker = Faker\Factory::create();

    	//za svakog studenta 
    	foreach ($students as $student) {
	    	//prvi put se upisao na studije 01.10. godine kojom mu pocinje broj indeksa
			$indexYear = substr($student->index_number, 0, 4);
			$date = new DateTime($indexYear.'-10-01');
			$date = $date->format('Y-m-d');
			DB::table('enrollments')->insert([
			            'student_id' => $student->id,
			            'student_year' => 1,
			            'renewal' => false,
			            'enrollment_date' => $date,
	                    'created_at' => Carbon::now(), 
	                    'updated_at' => Carbon::now()
		    ]);
			$x = 1;
			$y = 1;
			
			//za svaku naredno godinu 
	        for($i=$indexYear+1; $i<=2017; $i++) 
	        {
		        //biramo random godinu upisa studenta (da li je opet upisao istu, ili narednu)
		        //najvisa je 4. godina studija
		        $x = $faker->randomElement([$x, $x+1]);
		    	$x = ($x > 4) ? 4: $x;
		    	//ako je nova upisana godina (x) ista kao prethodna (y) u pitanju je obnova godine
		    	$renewal = ($x == $y) ? true : false;
		    	//datum upisa je 01.10. trenutne godine
		    	$date = new DateTime($i.'-10-01');
				$date = $date->format('Y-m-d');

		        DB::table('enrollments')->insert([
			        'student_id' => $student->id,
			        'student_year' => $x,
			        'renewal' => $renewal,
			        'enrollment_date' => $date,
	                'created_at' => Carbon::now(), 
	                'updated_at' => Carbon::now()
		        ]);

		        //trenutna upisana godina postaje prethodna u novom prolazu kroz petlju
		        $y = $x;                 
	        }


    	}
    }
}

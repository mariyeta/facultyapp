<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Student;
use App\Subject;
use Cache;
use Illuminate\Support\Facades\Input;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $student;
    protected $qualified;
    protected $unqualified;
    protected $failed;
    protected $added;

    //Cacheramo osnovne podatke o studentu
    public function __construct()
    {
        $this->middleware(function ($request, $next) {  
            //ucitavanje svih podataka o studentu
            $this->setAll();
            
            return $next($request);
        });
    }

    //Pocetna strana za studenta
    public function index()
    {
        $name = $this->student->first_name." ".$this->student->last_name;
        return view('student.main', compact('name'));
    }

    //Prikaz osnovnih podataka o studentu
    public function personal()
    {  
       return view('student.personal')->with('student', $this->student);
    }

    //Podaci o svim predmetima studenta
    public function subjects()
    {   
        return view('student.subjects')->with('student', $this->student);
    }

    //Podaci o ocenama studenta (u view-u se prikazuju samo polozeni ispiti)
    public function grades()
    {    
        return view('student.grades')->with(['student' => $this->student, 'qualified' => $this->qualified]);
    }


    //Podaci o svim neuspesnim polaganjima studenta
    public function failed()
    {  
        return view('student.failed')->with(['student' => $this->student, 'failed' => $this->failed]);
    }

    //Podaci o nepolozenim ispitima
    public function unqualified()
    {  
        return view('student.unqualified')->with(['student' => $this->student, 'unqualified' => $this->unqualified]);
    }

    //Svi upisi studenta
    public function enrollments()
    {  
        return view('student.enrollments')->with('student', $this->student);
    }

    //Prijava ispita
    public function createExaminations()
    {  
       //$subjects_exam - polaganja svih studentovih predmeta u trenutnom roku (mart 2018.)
       $subjects_exam = Cache::remember('subjects_exam_'.auth()->user()->id, 60, function () {
            return Student::with('user')->where('students.user_id', auth()->user()->id)->first()->load(['subjects.exams' => function($query) {
                $query->where('exams.exam_date','LIKE','2018-03-%');
            }]);
        });

         return view('student.examinations')->with(['subjects' => $subjects_exam, 'unqualified' => $this->unqualified, 'added' => $this->added]);
    }


    //upisujemo u bazu prijavljene ispite
    public function storeExaminations(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'checkbox' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('student/examinations')
                        ->withErrors(['message' => 'Molimo izaberite predmete koje želite da prijavite!']);
        }


        $input = Input::all();
        //u $checkboxes smestamo id-eve svih izabranih ispita
        $checkboxes = $input['checkbox'];

        //upisujemo izabrane ispite u tabelu exam_students
        foreach($checkboxes as $val)
        {
            \DB::table('exam_students')->insert([
                'student_id' => $this->student->id, 
                'exam_id' => $val
            ]);     
        }
        
        //azuriramo promenljivu $this->student
       $this->editStudent();
       return redirect('student/examinations');
    }


    //odjavljujemo (brisemo) ispit iz baze
    public function destroyExaminations(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'checkbox' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('student/examinations')
                        ->withErrors(['message' => 'Molimo izaberite predmete koje želite da prijavite!']);
        }

        $input = Input::all();
        //u $checkboxes smestamo id-eve svih izabranih ispita
        $checkboxes = $input['checkbox'];

        //brisemo izabrane ispite iz tabele exam_students
        foreach($checkboxes as $val)
        {
            \DB::table('exam_students')->where('exam_id', $val)->delete(); 
        }

        //azuriramo promenljivu $this->student
        $this->editStudent();

       return redirect('student/examinations');
        
    } 

    
    //Izbor predmeta
    //U $choose_subjects smestamo sve predmete poslednje godine koju je student upisao sa studentovog modula
    public function createSubjects()
    {
        //poslednja godina koju je student upisao i id studentovog modula
        $year = $this->student->enrollments->first()->student_year;
        $m_id = $this->student->module_id;

        //svi predmeti sa modula $m_id i godine $year ($year je 1,2,3 ili 4)
        $choose_subjects = Cache::remember('dep_'.$this->student->module->department_id.'_mod_'.$m_id.'_year_'.$year, 525600, function () use ($year, $m_id) {
            $q = function($query) use ($m_id){ $query->where('modules.id', $m_id);  };
            return Subject::whereHas('modules', $q)->with(['modules' => $q])->where('subjects.student_year', '=', $year)->get();
        });

        //u $student_year smestamo sve studentske godine za koje je student izabrao predmete
        $student_year = [];
        foreach($this->student->subjects as $subject) {
            $student_year[] = $subject->student_year;
        }
        $student_year = array_values(array_unique($student_year));
        
        return view('student.subjects_create')->with(['student' => $this->student, 'choose_subjects' => $choose_subjects, 'student_year' => $student_year]);
    } 


    //upisujemo u bazu izabrane predmete
    public function storeSubjects(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'obligatory.*' => 'required',
            'radio' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('student/subjects/create')
                        ->withErrors(['message' => 'Molimo izaberite jedan od ponuđenih predmeta!']);
        }

        $input = Input::all();
        //u $radio je smesten id izabranog predmeta, a $obligatory je niz sa id-evima obaveznih predmeta
        $radio = $input['radio'];
        $obligatory = $input['obligatory'];

        //upisujemo izabrane predmete u tabelu student_subject
        foreach($obligatory as $val)
        {
            $this->student->subjects()->attach($val);
        }
        $this->student->subjects()->attach($radio);
        //brisemo cache za subjects_exam_$id jer je student prijavio nove predmete pa i njih treba uzeti u obzir
        Cache::forget('subjects_exam_'.auth()->user()->id);
        //azuriramo promenljivu $this->student
        $this->editStudent();
                
       return redirect('student/subjects/create');
    }


    public function destroySubjects()
    {
        //brisemo sve izabrane predmete studenta za poslednju upisanu skolsku godinu
        $subjects_from_year = $this->student->subjects->where('student_year', $this->student->enrollments->first()->student_year)->pluck('id');
        \DB::table('student_subject')->where('student_id', $this->student->id)->whereIn('subject_id', $subjects_from_year)->delete();
        //brisemo cache za subjects_exam_$id jer je student prijavio nove predmete
        Cache::forget('student_exams_'.auth()->user()->id);
        //azuriramo promenljivu $this->student
        $this->editStudent();

       return redirect('student/subjects/create');
        
    } 


    //f-ja azurira promenljivu $this->student
    public function editStudent()
    {
        Cache::forget('student_'.auth()->user()->id.'_all');
        $this->setAll();
    } 


    //u promenljivu $student se smestaju svi podaci o studentu
    public function setAll() {
            $this->student = Cache::remember('student_'.auth()->user()->id.'_basic', 60, function () {
                return Student::with('user', 'module.department')->where('students.user_id', auth()->user()->id)->first();
            });

            $this->student = Cache::remember('student_'.auth()->user()->id.'_all', 60, function () {
                return $this->student->load(['subjects.modules' => function ($query) {
                    $query->where('module_subject.module_id', $this->student->module->id);
                }])->load(['enrollments' => function($query) {
                    $query->orderBy('enrollment_date', 'desc');
                }])->load('examStudents.exam.subject', 'examStudents.grades.professor');
            });
            $this->qualifiedExams();
            $this->unqualifiedExams();
            $this->addedExams();
    }

    //id predmeta koje je student polozio
    public function qualifiedExams() {
        $this->qualified = [];
        $this->failed = [];
        foreach($this->student->examStudents as $grade) {
            if(!empty($grade->grades->first())) {
                if($grade->grades->first()->grade > 5){
                 $this->qualified[] = $grade->exam->subject->id;
                }
                else {
                    $this->failed[] = $grade->id;
                }
            }
        }
    }

    //id predmeta koje student nije polozio
    public function unqualifiedExams() {
        $this->qualifiedExams();
        $this->unqualified = [];
        foreach($this->student->subjects as $subject) {
            if(!in_array($subject->id, $this->qualified)){
                $this->unqualified[] = $subject->id;
            }
        }
    }

    //id ispita koje je student prijavio u tekucem roku
    public function addedExams() {
        $this->added = [];
        foreach($this->student->examStudents as $exam) {
            if($exam->grades->count() < 1) {
               $this->added[] = $exam->exam_id;
            }
        }
    }
    


}
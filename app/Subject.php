<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public $timestamps = false;

     //Many-to-many
    public function modules()
    {
        return $this->belongsToMany('App\Module')->withPivot('status');
    }
    
    //Many-to-many
    public function professors()
    {
        return $this->belongsToMany('App\Professor')->withPivot('norm', 'enrollment_year');
    }

    //Many-to-many
     public function students()
    {
        return $this->belongsToMany('App\Student');
    }

    //One-to-many
    public function exams() {
        return $this->hasMany('App\Exam');
   }

    

}

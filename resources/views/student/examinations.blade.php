
@extends('layouts.app')

@section('content')
@if(Auth::check()) 

  <h1 class="text-center">Prijava ispita</h1>
  <hr>
  <div class="row">
    <div class="col-12 text-center">
      @if(empty($unqualified)) 
        <h4>Ne postoje ispiti koje možete da prijavite</h4>
      @else
        <h4>Ispiti koje možete da prijavite</h4>
        <form class="form-signin" method="POST" action="/student/examinations">
            {{ csrf_field() }}
          <table class="table table-bordered text-left subject_table">
            <tbody>
              <tr>
                 @include('layouts.examination_table')
              </tr>
              @foreach($subjects->subjects as $subject)
                @if(in_array($subject->id, $unqualified) && !in_array($subject->exams->first()->id, $added))
                <tr>
                  @include('layouts.examination_table_results')
                </tr>
                @endif
              @endforeach
            </tbody>
          </table>
          <input type="submit" id="add_examinations" name="add_examinations" class="btn btn-primary" value="Prijavi ispite"></td>
        </form>

        @if(!empty($added))
        <br><br>
          <h4>Ispiti koje ste prijavili</h4>
          <form class="form-signin" method="POST" action="/student/examinations">
            <input name="_method" type="hidden" value="DELETE">
              {{ csrf_field() }}
            <table class="table table-bordered text-left subject_table">
              <tbody>
                <tr>
                   @include('layouts.examination_table')
                </tr>
                @foreach($subjects->subjects as $subject)
                  @if(in_array($subject->id, $unqualified) && in_array($subject->exams->first()->id, $added))
                  <tr>
                    @include('layouts.examination_table_results')
                  </tr>
                  @endif
                @endforeach
              </tbody>
            </table>
            <input type="submit" id="remove_examinations" name="remove_examinations" class="btn btn-primary" value="Odjavi ispite"></td>
          </form>
        @endif
    @include('layouts.errors')
    @endif
    </div>
  </div>
    
@endif
@endsection
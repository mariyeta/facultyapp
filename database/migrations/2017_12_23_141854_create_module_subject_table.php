<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleSubjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_subject', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('module_id')->unsigned();
            $table->integer('subject_id')->unsigned();
            $table->string('status', 1);
        });

        Schema::table('module_subject', function($table) {
            $table->foreign('module_id')->references('id')->on('modules');
            $table->foreign('subject_id')->references('id')->on('subjects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_subject');
    }
}

<?php

use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
  
        //Kreiramo 6 studenata
    	factory(App\Student::class, 6)->create();
    }
}

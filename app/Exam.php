<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    public $timestamps = false;

	//One-to-many
    public function subject() {
        return $this->belongsTo('App\Subject');
   	}

    /*
   	//Many-to-many
   public function students()
    {
        return $this->belongsToMany('App\Student');
    }
    */

  //One-to-many
    public function examStudents()
    {
        return $this->hasMany('App\ExamStudent', 'exam_id', 'id');
    }



}




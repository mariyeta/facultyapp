<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Za svaki department (u ovom slucaju kreiramo 1), kreiraj 2 modula
       $departments = factory(App\Department::class, 1)->create()->each(function($u) {
           $modules = factory(App\Module::class, 2)->create(['department_id' => $u->id]);
        });

    }
}

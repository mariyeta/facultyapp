
@extends('layouts.app')

@section('content')
@if(Auth::check()) 

  <h1 class="text-center">Moji predmeti</h1>
  <hr>
  <div class="row">
    <div class="col-12 text-center">
      @if(count($student->subjects))
        <table class="table table-bordered text-left subject_table">
          <tbody>
            <tr>
              @include('layouts.subject_table')
            </tr>
            @foreach($student->subjects as $subject)
            <tr>
              @include('layouts.subject_table_results')
            </tr>
            @endforeach
          </tbody>
        </table>
      @else
        <h4>Ne postoje odabrani predmeti</h4>
      @endif
    </div>
  </div>
    
@endif
@endsection
   
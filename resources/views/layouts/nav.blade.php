<nav class="navbar navbar-inverse navbar-toggleable-md fixed-top bg-primary navbar-dark">
   <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <!-- Brand -->
    <a class="navbar-brand" href="#">Fakultetski servisi</a>

    <!-- Links -->
    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav ml-auto">
        <!-- Dropdown -->
        @if (Auth::check())
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ Auth::user()->name }}
          </a>
          <div class="dropdown-menu" aria-labelledby="navbardrop">
            <a class="dropdown-item" href="/logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Odjavi se</a>
            <form class="hidden_form" id="logout-form" action="/logout" method="POST">
                {{ csrf_field() }}
            </form>
          </div>
        </li>
        @endif
      </ul>
  </div>
</nav>

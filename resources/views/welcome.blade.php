
@extends('layouts.main')

@section('content')

      <div class="inner cover">
            <h1 class="cover-heading text-primary">Fakultetski servisi</h1>
            <p class="lead">
              <a class="btn btn-primary" href="/login" role="button">Prijavi se</a>
            </p>
      </div>
    @endsection


@extends('layouts.app')

@section('content')
@if(Auth::check()) 

<h1 class="text-center">Izbor predmeta</h1>
  <hr>
  <div class="row">
    <div class="col-12 text-center">
      {{-- Ako je student obnovio godinu, ne dozvoljava mu se izbor predmeta (predmeti se biraju kada se prvi put upisuje godina) --}}
      @if($student->enrollments->first()->renewal == 1)
        <h4>Ne postoji mogućnost odabira predmeta</h4>
        <h4>Odabrani predmeti iz {{ $student->enrollments->first()->student_year }}. godine</h4>
        <table class="table table-bordered text-left subject_table">
        <tbody>
          <tr>
            @include('layouts.subject_table')
          </tr>
          @foreach($student->subjects->where('student_year', $student->enrollments->first()->student_year) as $subject)
          <tr>
            @include('layouts.subject_table_results')
          </tr>
          @endforeach
        </tbody>
      </table>

      {{-- Ako je student prvi put upisao godinu, a vec je odabrao predmete, dozvoljava mu se da azurira izbor --}}
      @elseif(in_array($choose_subjects->first()->student_year, $student_year))
        <h4>Već ste odabrali predmete</h4>
        <h4>Odabrani predmeti iz {{ $student->enrollments->first()->student_year }}. godine</h4>
        <form class="form-signin" method="POST" action="/student/subjects/create">
          <input name="_method" type="hidden" value="DELETE">
        {{ csrf_field() }}
        <table class="table table-bordered text-left subject_table">
        <tbody>
          <tr>
               @include('layouts.subject_table')
          </tr>
          @foreach($student->subjects->where('student_year', $student->enrollments->first()->student_year) as $subject)
          <tr>
            @include('layouts.subject_table_results')
          </tr>
          @endforeach
        </tbody>
      </table>
      <br>
      <input type="submit" id="edit_subject" name="edit_subject" class="btn btn-primary" value="Izmeni predmete"></td>
      </form>

      {{-- U suprotnom (student je prvi put upisao godinu, i nije izabrao predmeta) pojavljuje mu se forma za odabir predmeta --}}
      {{-- Obavezni predmeti se svakako unose u bazu, student bira jedan od dva ponudjena izborna predmeta --}}
      @else
      <form class="form-signin" method="POST" action="/student/subjects/create">
        {{ csrf_field() }}
      <h4>Obavezni predmeti iz {{ $student->enrollments->first()->student_year }}. godine</h4>
      <table class="table table-bordered text-left subject_table">
        <tbody>
          <tr>
              <th class="subject_width3" title=""></th>
              @include('layouts.subject_table')
          </tr>
          @foreach($choose_subjects as $subject)
             @if($subject->modules->first()->pivot->status == 'O')
          <tr>
          	<td><input name="obligatory[]" type="hidden" value="{{ $subject->id }}"></td>
            @include('layouts.subject_table_results')
          </tr>
          	@endif
          @endforeach
        </tbody>
      </table>
      <br>
      <h4>Izborni predmeti iz {{ $student->enrollments->first()->student_year }}. godine</h4>
      <h6>Izaberite jedan od ponuđenih predmeta</h6>
      <table class="table table-bordered text-left subject_table">
        <tbody>
		      <tr>
               <th class="subject_width3" title=""></th>
              @include('layouts.subject_table')
          </tr>
          @foreach($choose_subjects as $subject)
          @if($subject->modules->first()->pivot->status == 'I')
          <tr>
          	<td><input name="radio" type="radio" value="{{ $subject->id }}"></td>
            @include('layouts.subject_table_results')
          </tr>
          	@endif
          @endforeach
        </tbody>
      </table>
      <br>
      <input type="submit" id="choose_subject" name="choose_subject" class="btn btn-primary" value="Potvrdi"></td>
      </form>
      @include('layouts.errors')
      @endif
    </div>
  </div>
  
@endif
@endsection






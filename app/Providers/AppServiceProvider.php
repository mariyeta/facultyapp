<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Auth;
use Cache;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

      //Svuda gde se ucitava layouts.sidebar.blade.php file posalji varijablu $options koju vraca f-ja sidebar()
      view()->composer('layouts.sidebar', function ($view) {
            $view->with('options', \App\Option::sidebar());
      });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DepartmentsTableSeeder::class);
        $this->call(TitlesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SubjectsTableSeeder::class);
        $this->call(StudentsTableSeeder::class);
        $this->call(ProfessorsTableSeeder::class);
        $this->call(ProfessorHistoryTableSeeder::class);
        $this->call(EnrollmentsTableSeeder::class);
        $this->call(StudentSubjectTableSeeder::class);
        $this->call(ExamStudentTableSeeder::class);
        $this->call(GradesTableSeeder::class);
        $this->call(OptionsTableSeeder::class);
       }
}

@extends('layouts.main')

@section('content')
    <form class="form-signin" method="POST" action="/login">
        {{ csrf_field() }}
        <label for="name" class="sr-only">Korisničko ime</label>
        <input type="text" name="name" id="name" class="form-control" placeholder="Korisničko ime" required autofocus>
        <label for="password" class="sr-only">Lozinka</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Lozinka" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Prijavi se</button>
        @include('layouts.errors')
    </form>

 @endsection

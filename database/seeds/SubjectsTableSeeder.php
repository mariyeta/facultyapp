<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    //Za svaku od navedenih godina biramo random datum i vreme ispita u 3., 6. i 9. mesecu (svaki ispit ima termin polaganja u sva tri roka svake godine)
    public static function randDate () {
		for($year=2012; $year<=2018; $year++) {
			if($year==2018) {
				$months = [3];
			}
			else {
				$months = [3, 6, 9];
			}
			foreach($months as $month) {
			$day = mt_rand(1, 28);
			$hour = [8, 11, 14, 17, 20];
			$h = array_rand($hour);
			$min = [0, 30];
			$m = array_rand($min);
			$date[] = Carbon::create($year, $month, $day, $hour[$h], $min[$m], 0);
			}	
		}
		return $date;
	}

	//Unos ispita u bazu
	public static function insertExams($id) {
		$dates = self::randDate();
		foreach($dates as $date) {	    	
			DB::table('exams')->insert([
			'subject_id' => $id, 
			'exam_date' => $date
			]);					
		}
	}



    public function run()
    {

    	//Za svaku od cetiri godine studiranja, za svaki department kreiramo: 
    	//a) jedan obavezni predmet koji se vezuje za oba modula departmana
    	//b) za svaki module departmana po jedan obavezni i c) dva izborna predmeta
    	//Odmah unosimo datum i vreme polaganja svakog ispita (f-ja insertExams) za svaku godinu izmedju 2012 i 2017, za svaki rok (u 3., 6. i 9. mesecu)

		for($i=1; $i<5; $i++) {
			$count = App\Department::count();

			for($k=1; $k<=$count; $k++) {
			    $faker = Faker\Factory::create();
			    $modules = App\Module::where('department_id', $k)->pluck('id');
				// a)
				$subjects = factory(App\Subject::class, 1)->create(array('credit' => 20, 'student_year' => $i))->each(function ($subject) use ($modules, $faker)
				{ 
					self::insertExams($subject->id);
					foreach ($modules as $module) {
						$subject->modules()->attach($module, ['status' => 'O']); 
					}
				});

				foreach ($modules as $module) {
					// b)
					$subjects = factory(App\Subject::class, 1)->create(array('credit' => 20, 'student_year' => $i))->each(function ($subject) use ($module, $faker) { 
						$subject->modules()->attach($module, ['status' => 'O']); 
						self::insertExams($subject->id);	
					});

					// c)
					$subjects = factory(App\Subject::class, 2)->create(array('credit' => 20, 'student_year' => $i))->each(function ($subject) use ($module, $faker) { 
						$subject->modules()->attach($module, ['status' => 'I']); 	
						self::insertExams($subject->id);
					});
				}
			}
		} 

    }


}



<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
use Carbon\Carbon;



/** @var \Illuminate\Database\Eloquent\Factory $factory */

//U factory-u User-a definisemo email adresu koja je unique
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    return [
       // 'name' => $faker->unique()->userName,
        'email' => $faker->unique()->safeEmail,
       // 'password' => $password ?: $password = bcrypt('secret'),
       // 'role_id' => DB::table('roles')->inRandomOrder()->first()->id,
        'remember_token' => str_random(10),
    ];
});
//Factory za Studenta
$factory->define(App\Student::class, function (Faker\Generator $faker) {
    //ucitavamo MyMethods Provider u kom su definisane f-ja jmbg() i indexNumber()
    $faker->addProvider(new \Faker\Provider\MyMethods($faker));
    //$dob - date of birth - random datum izmedju '93. i '95.
    $dob = $faker->dateTimeBetween($startDate = "1993-01-01", $endDate = "1995-12-31")->format('Y-m-d');
    $year = Carbon::createFromFormat('Y-m-d', $dob)->year;
    //uzimamo user_id svih sa ulogom studenta
    $users = DB::table('users')->where('role_id', 3)->orderBy('id', 'asc')->pluck('id')->all();
    //nalazimo indexNumber u obliku godina+19/random broj do 1000 (npr. 2015/0023)
    $indexNumber = $faker->unique()->indexNumber($year);
    //random pol
    $gender = $faker->randomElement(['male', 'female']);
    $gender_srb = ($gender == 'female') ? 'ž' : 'm';
    //moguci moduli
    $modules = DB::table('modules')->pluck('id')->toArray();

    return [
    	'user_id' => $faker->unique()->randomElement($users),
        'module_id' => $faker->randomElement($modules),
        'first_name' => $faker->firstName($gender),
        'last_name' => $faker->lastName,
        'parent_name' => $faker->firstName,
        'date_of_birth' => $dob,
        'jmbg' => $faker->unique()->jmbg($dob),
        'index_number' => $indexNumber,
        'street_address' => $faker->address,
        'gender' => $gender_srb
    ];
});

//Factory za Subject
$factory->define(App\Subject::class, function (Faker\Generator $faker) {
    return [
            'name' => $faker->unique()->sentence(3),
            'acronym' => $faker->unique()->sentence(1),
            'lecture' => $faker->randomElement([1, 2, 3]),
            'exercise' => $faker->randomElement([1, 2, 3]),
            'lab' => $faker->randomElement([1, 2, 3])
    ];
    
});

//Factory za profesora
$factory->define(App\Professor::class, function (Faker\Generator $faker) {
   //ucitavamo MyMethods Provider u kom su definisane f-ja jmbg() i indexNumber()
    $faker->addProvider(new \Faker\Provider\MyMethods($faker));
    //$dob - date of birth - random datum izmedju '50. i '74.
    $dob = $faker->dateTimeBetween($startDate = "1950-01-01", $endDate = "1974-12-31")->format('Y-m-d');
    //uzimamo user_id svih sa ulogom profesora
    $users = DB::table('users')->where('role_id', 2)->orderBy('id', 'asc')->pluck('id')->all();
    //random pol
    $gender = $faker->randomElement(['male', 'female']);
    $gender_srb = ($gender == 'female') ? 'ž' : 'm';
    return [
    	'user_id' => $faker->unique()->randomElement($users),
        'first_name' => $faker->firstName($gender),
        'last_name' => $faker->lastName,
        'parent_name' => $faker->firstName,
        'date_of_birth' => $dob,
        'jmbg' => $faker->unique()->jmbg($dob),
        'street_address' => $faker->address,
        'gender' => $gender_srb,
        'title_id' => DB::table('titles')->inRandomOrder()->first()->id,
        'from_date' => $faker->dateTimeBetween($startDate = "2013-01-01", $endDate = "2016-12-31")->format('Y-m-d')
    ];
});


//Department ima samo polje 'name', koje je unique 
$factory->define(App\Department::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->sentence(2)
    ];
});

//Modul ima samo polje 'name', koje je unique 
$factory->define(App\Module::class, function (Faker\Generator $faker) {   
    return [
        'name' => $faker->unique()->sentence(2)
    ];
});

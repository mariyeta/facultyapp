<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon; 

class StudentSubjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = App\Student::all();

        //za svakog studenta nalazimo trenutnu godinu na kojoj je upisan i da li je godina obnovljena
    	foreach ($students as $student) {
    		$s = \DB::table('enrollments')->where('student_id', $student->id)->orderBy('enrollment_date', 'desc')->first();
            $year = $s->student_year;
            $renewal = $s->renewal;


                //Ukoliko je godina prvi put upisana ($renewal=0), ne unosimo predmete za tu godinu
                if($renewal == 1) {
                    $y = $year;
                } else {
                    $y = $year - 1;
                }
                //za studentske godine
        		for ($i=1; $i<=$y; $i++) {
                    //nalazimo obavezne predmete na modulu studenta i povezujemo studenta sa tim predmetima
    				$match_o = ['subjects.student_year' => $i, 'module_subject.status' => 'O', 'module_subject.module_id' => $student->module_id];
    				$subjects_o = \DB::table('subjects')->join('module_subject','subjects.id','=','module_subject.subject_id')->where($match_o)->pluck('subjects.id')->toArray();
    				foreach($subjects_o as $subject) {
    					DB::table('student_subject')->insert([
    		            'student_id' => $student->id,
    		            'subject_id' => $subject
    	    			]);
    				}
        		
                    //nalazimo izborne predmete na modulu studenta i dodeljujemo mu 1
        			$match_i6 = ['subjects.student_year' => $i, 'module_subject.status' => 'I', 'module_subject.module_id' => $student->module_id];
    				$subjects_i6 = \DB::table('subjects')->join('module_subject','subjects.id','=','module_subject.subject_id')->where($match_i6)->pluck('subjects.id')->toArray();
    				$keys_i6 = array_rand($subjects_i6);
    				DB::table('student_subject')->insert([
    		            'student_id' => $student->id,
    		            'subject_id' => $subjects_i6[$keys_i6]
    	    		]);
            	}
            
        }
    }
}

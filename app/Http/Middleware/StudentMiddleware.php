<?php

namespace App\Http\Middleware;

use Closure;

class StudentMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    //Ako user nije ulogovan ili nije student, vrati ga na prethodnu stranicu, u suprotnom dozvoli mu pristup opciji
    public function handle($request, Closure $next)
    {
        
     if (!$request->user() || ($request->user() && $request->user()->role_id != 3))
        {
            return back();        }

    
        return $next($request);
    }
}

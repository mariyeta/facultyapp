<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TitlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        //U bazu unosimo zvanja od najnizeg ka najvisem
        DB::table('titles')->insert([
            ['name' => 'Saradnik u nastavi'],
            ['name' => 'Asistent'],
            ['name' => 'Docent'],
            ['name' => 'Vanredni profesor'],
            ['name' => 'Redovni profesor'],
            ['name' => 'Emeritus']
        ]);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enrollment extends Model
{
  //Many-to-many
   public function students()
    {
        return $this->belongsTo('App\Student');
    }

}
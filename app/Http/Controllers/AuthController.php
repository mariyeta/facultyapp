<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
   protected $fillable = ['name', 'password'];


   //Samo gost ima pristup svim opcijama (osim create i destroy)
   public function __construct() {
   		$this->middleware('guest', ['except' => ['create', 'destroy']]);
   }

   //Ukoliko je korisnik vec ulogovan, redirektuje se na odgovarajucu pocetnu stranicu u zavisnosti od uloge, u suprotnom mu se prikazuje forma za logovanje
  public function create() {
      if(auth()->user()) {
        return $this->checkUser();
      }
      else {
      	return view('auth.login');
      }
    
  }

  //U slucaju neuspesnog pokusaja logovanja ispisuje poruku korisniku, u suprotnom ga loguje i redirektuje na odgovarajucu stranicu na osnovu njegove uloge
  public function store() {
    if (!auth()->attempt(request(['name', 'password']))) {
    	return back()->withErrors(['message' => 'Pogrešno korisničko ime i/ili lozinka']);
    }

    return $this->checkUser();
  }

  //Opcija omogucava korisniku da se izloguje i vraca ga na pocetnu stranicu servisa
  public function destroy() {
    auth()->logout(); 
  return redirect()->home();
  }

  //proverava ulogu korisnika i redirektuje na odgovarajucu stranicu
  public function checkUser() {
    if(auth()->user()->isProfessor()){
        return redirect()->route('professor');
    } elseif(auth()->user()->isStudent()){
        return redirect()->route('student');
    } elseif(auth()->user()->isAdmin()){
      return redirect()->route('admin');
    }
  }


}

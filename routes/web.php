<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Middleware\AdminMiddleware;
use App\Http\Middleware\StudentMiddleware;
use App\Http\Middleware\ProfessorMiddleware;


Route::get('/servisi', function () {
    return view('welcome');
})->name('home');

//Opcijama za logovanje
Route::get('/login', 'AuthController@create');
Route::post('/login', 'AuthController@store');
Route::post('/logout', 'AuthController@destroy');

//Samo student ima pristup sledecim opcijama 
Route::group(['middleware' => 'App\Http\Middleware\StudentMiddleware'], function () {
    Route::get('/student', 'StudentController@index')->name('student');
    Route::get('/student/personal', 'StudentController@personal');
    Route::get('/student/subjects', 'StudentController@subjects');
    Route::get('/student/grades', 'StudentController@grades');
    Route::get('/student/failed_exams', 'StudentController@failed');
    Route::get('/student/subjects/create', 'StudentController@createSubjects');
    Route::post('/student/subjects/create', 'StudentController@storeSubjects');
    Route::delete('/student/subjects/create', 'StudentController@destroySubjects');
    Route::get('/student/unqualified_exams', 'StudentController@unqualified');
    Route::get('/student/examinations', 'StudentController@createExaminations');
    Route::post('/student/examinations', 'StudentController@storeExaminations');
    Route::delete('/student/examinations', 'StudentController@destroyExaminations');
    Route::get('/student/enrollments', 'StudentController@enrollments');
});

//Samo profesor ima pristup sledecim opcijama 
Route::group(['middleware' => 'App\Http\Middleware\ProfessorMiddleware'], function () {
    Route::get('/professor', 'ProfessorController@index')->name('professor');
    Route::get('/professor/personal', 'ProfessorController@personal');
    Route::get('/professor/previous_titles', 'ProfessorController@previousTitles');
});

//Samo admin ima pristup sledecim opcijama 
Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function () {
    Route::get('/admin', 'AdminController@index')->name('admin');
    Route::get('/admin/admin2', 'AdminController@admin2');
});




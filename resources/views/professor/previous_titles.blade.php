
@extends('layouts.app')

@section('content')
@if(Auth::check()) 

  <h1 class="text-center">Prethodna zvanja</h1>
  <hr>
  <div class="row">
    <div class="col-12 text-center">
      @if(count($titles))
      <table class="table table-bordered text-left subject_table">
        <tbody>
          <tr>
            <th title="Zvanje">Zvanje</th>
            <th title="Datum početka zvanja">Od datuma</th>
            <th title="Datuma prestanka zvanja">Do datuma</th>
            <th title="Reizbor">Reizbor</th>
          </tr>
          @foreach($titles as $title)
          <tr>
            <td>{{ $title->name }}</td>
            <td>{{ Carbon\Carbon::parse($title->from_date)->format('d.m.Y.') }}</td>
            <td>{{ Carbon\Carbon::parse($title->to_date)->format('d.m.Y.') }}</td>
            <td>{{ $title->reappointment == 0 ? "Ne" : "Da" }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
      @else
        <h4>Ne postoje prethodna zvanja</h4>
      @endif
    </div>
  </div>
    
@endif
@endsection
   
@if (count($errors))

<hr>
<div class="form-group">
	<div class="alert alert-warning">
	  <ul class="list-unstyled">
	  	@foreach($errors->all() as $error)
	  	<li>{{ $error }}</li>
	  	@endforeach
	  </ul>
	</div>
</div>

@endif

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamStudent extends Model
{
    public $timestamps = false;

    //One-to-many
   public function student()
    {
        return $this->belongsTo('App\Student');
    }

    //One-to-many
    public function exam()
    {
        return $this->belongsTo('App\Exam');
    }

    //One-to-many
    public function grades()
    {
        return $this->hasMany('App\Grade');
    }


    public function scopeExamDate($query)
    {
        return $query->orderBy('exam_date', 'asc');
    }
}




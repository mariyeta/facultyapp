$(document).ready(function () {
	var url = window.location.pathname;
	var pos = url.indexOf('/');
	var name = url.substr(pos);
	//dodeljujemo klasu active trenutnom linku
	$('div.list-group>a.list-group-item.active').removeClass('active');
	$('a[href="'+name+'"]').addClass('active');

	//proverava da li je izabran izborni predmet
	$("#choose_subject").click(function(){
            var name = $("input:radio").attr("name");
            if($("input:radio[name="+name+"]:checked").length > 0){  
            	return true;
            }
            alert('Molimo izaberite jedan od ponuđenih predmeta!');
           return false;      
    });

	//proverava da li je oznacen bar jedan ispit koji se prijavljuje
	$("#add_examinations").click(function(){
            if ($("input[type='checkbox']:checked").length < 1){
		        alert("Molimo izaberite predmete koje želite da prijavite!");
				return false;
				}
           return true;      
    });

	//proverava da li je oznacen bar jedan ispit koji se odjavljuje
    $("#remove_examinations").click(function(){
            if ($("input[type='checkbox']:checked").length < 1){
		        alert("Molimo izaberite predmete koje želite da odjavite!");
				return false;
				}
           return true;      
    });


  });
<?php

use Illuminate\Database\Seeder;
use App\Role;
use Carbon\Carbon;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Sidebar opcije u zavisnosti od uloge
        DB::table('options')->insert([
            ['role_id' => '1', 'href' => '/admin', 'description' => 'Admin1'],
            ['role_id' => '1', 'href' => '/admin/admin2', 'description' => 'Admin2'],
            ['role_id' => '2', 'href' => '/professor', 'description' => 'Početna'],
            ['role_id' => '2', 'href' => '/professor/personal', 'description' => 'O profesoru'],
            ['role_id' => '2', 'href' => '/professor/previous_titles', 'description' => 'Prethodna zvanja'],
            ['role_id' => '3', 'href' => '/student', 'description' => 'Početna'],
            ['role_id' => '3', 'href' => '/student/personal', 'description' => 'O studentu'],
            ['role_id' => '3', 'href' => '/student/subjects', 'description' => 'Moji predmeti'],
            ['role_id' => '3', 'href' => '/student/subjects/create', 'description' => 'Izbor predmeta'],
            ['role_id' => '3', 'href' => '/student/grades', 'description' => 'Ocene'],
            ['role_id' => '3', 'href' => '/student/unqualified_exams', 'description' => 'Nepoloženi ispiti'],
            ['role_id' => '3', 'href' => '/student/failed_exams', 'description' => 'Neuspešna polaganja'],
            ['role_id' => '3', 'href' => '/student/examinations', 'description' => 'Prijava ispita'],
            ['role_id' => '3', 'href' => '/student/enrollments', 'description' => 'Upisi']
        ]);
    }
}

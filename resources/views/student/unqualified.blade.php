
@extends('layouts.app')

@section('content')
@if(Auth::check()) 

  <h1 class="text-center">Nepoloženi ispiti</h1>
  <hr>
  <div class="row">
    <div class="col-12 text-center">
      @if(empty($unqualified))
        <h4>Ne postoje nepoloženi ispiti</h4>
      @else
      <table class="table table-bordered text-left subject_table">
        <tbody>
          <tr>
             @include('layouts.subject_table')
          </tr>
          @foreach($student->subjects as $subject)
            @if(in_array($subject->id, $unqualified))
            <tr>
              @include('layouts.subject_table_results')
            </tr>
            @endif
          @endforeach
        </tbody>
      </table>
      @endif
    </div>
  </div>
@endif
@endsection
   

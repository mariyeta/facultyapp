
@extends('layouts.app')

@section('content')
@if(Auth::check()) 
  <h1 class="text-center">Neuspešna polaganja</h1>
  <hr>
  <div class="row">
    <div class="col-12 text-center">
      @if(empty($failed))
      	<h4>Ne postoje neuspešna polaganja</h4>
      @else
	  	<table class="table table-bordered text-left subject_table">
	        <tbody>
	          <tr>
	          	@include('layouts.grade_table')
	          </tr>
	          @foreach($student->examStudents as $grade)
		          @if(!empty($grade->grades->first()))
			          @if ($grade->grades->first()->grade <= 5)
			          <tr>
			          	<td>{{ $grade->exam->subject->acronym }}</td>
			          	<td>{{ $grade->exam->subject->name }}</td>
			            <td>{{ Carbon\Carbon::parse($grade->exam->exam_date)->format('d.m.Y.') }}</td>
			            <td title="{{ $grade->grades->first()->grade == 0 ? 'Nije izašao' : '' }}">{{ $grade->grades->first()->grade == 0 ? "N" : "5" }}</td>
			            <td>{{ $grade->grades->first()->professor->first_name." ".$grade->grades->first()->professor->last_name }}</td>
			          </tr>
			          @endif
		          @endif
	          @endforeach
	        </tbody>
	    </table>
	  @endif
    </div>
 </div>
@endif
@endsection
<?php

use Illuminate\Database\Seeder;
use App\Role;
use Carbon\Carbon;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Moguce uloge korisnika
        DB::table('roles')->insert([
            ['name' => 'Administrator'],
            ['name' => 'Profesor'],
            ['name' => 'Student']
        ]);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public $timestamps = false;

     //Many-to-many
    public function subjects()
    {
        return $this->belongsToMany('App\Subject')->withPivot('status');
    }
    
    //One-to-many
    public function department() {
        return $this->belongsTo('App\Department');
   }

  //One-to-many
   public function students()
    {
        return $this->hasMany('App\Student');
    }


}




<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProfessorHistoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	$profs = App\Professor::all();
    	$faker = Faker\Factory::create();

        //za svakog profesora pravimo istoriju prethodnih zvanja
        //pretpostavka je da je prosao kroz sva zvanja od najnizeg do trenutnog
    	foreach ($profs as $prof) {
            //ukoliko je zvanje vece od najnizeg
			if($prof->title_id>1){
                //nalazimo dva datuma; d1 je pocetak trenutnog zvanja, a d2 je 5 godina pre toga
				$d1 = new DateTime($prof->from_date);
                $d2 = clone $d1;
				$d2 = $d2->modify('-5 year')->format('Y-m-d');

                //prethodno zvanje pocinje od d2 (pre 5 godina) a zavrsava se na d1 (pocetak trenutnog zvanja)
                for($i=1; $i<$prof->title_id; $i++) {
	        		DB::table('professor_history')->insert([
		            	'professor_id' => $prof->id,
		            	'title_id' => $prof->title_id - $i,
		            	'from_date' => $d2,
		            	'to_date' => $d1,
                        'created_at' => Carbon::now(), 
                        'updated_at' => Carbon::now()
	        		]);

                    //novo d1 postaje d2, a d2 umanjujemo za 5 godina
                    $d1 = $d2;
                    $d2 = new DateTime($d2);
                    $d2 = $d2->modify('-5 year')->format('Y-m-d');
        		}
        	}

    	}

    }
}

